import React, { Component } from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Awal from './components/Awal/Awal';
import Daftar from './components/Daftar/Daftar';
import Login from './components/Login/Login';
import { Provider } from 'react-redux';
import { store, persistor } from './store';
import jwt_decode from 'jwt-decode';
import setJWTToken from './securityUtils/setJWTToken';
import { SET_CURRENT_USER } from './actions/types';
import { logout } from './actions/securityActions';
import SecuredRoute from './securityUtils/SecureRoute';
import { PersistGate } from 'redux-persist/integration/react';
import PagePengunjung from './components/Pengunjung';
import PageAdmin from './components/Admin/index';
import TambahBuku from './components/Admin/Buku/TambahBuku';
import TampilBuku from './components/Admin/Buku/TampilBuku';
import './components/FontAwesome/index';
import Pengguna from './components/Admin/Pengguna/Pengguna';
import TambahAdmin from './components/Admin/Pengguna/TambahAdmin';
import DetailBuku from './components/Admin/Buku/DetailBuku';
import Profil from './components/Admin/Profil/Profil';
import TampilBukuP from './components/Pengunjung/Buku/TampilBukuP';
import EditBuku from './components/Admin/Buku/EditBuku';
import EditPengguna from './components/Admin/Pengguna/EditPengguna';
import DetailBukuP from './components/Pengunjung/Buku/DetailBukuP';
import Keranjang from './components/Pengunjung/Keranjang/Keranjang';
import ProfilP from './components/Pengunjung/Profil/ProfilP';
import EditProfil from './components/Admin/Profil/EditProfil';
import UbahPassword from './components/Admin/Profil/UbahPassword';
import DetailPengguna from './components/Admin/Pengguna/DetailPengguna';
import HomeAdmin from './components/Admin/HomeAdmin';
import Laporan from './components/Admin/Laporan/Laporan';
import Pengembalian from './components/Admin/Pengembalian/Pengembalian';
import DefaultPassword from './components/DefaultPassword';

const tokenUserLogin = localStorage.getItem('tokenUserLogin');

if (tokenUserLogin) {
  setJWTToken(tokenUserLogin);
  const decoded_tokenUserLogin = jwt_decode(tokenUserLogin);
  store.dispatch({
    type: SET_CURRENT_USER,
    payload: decoded_tokenUserLogin,
  });

  const currentTime = Date.now() / 1000;
  if (decoded_tokenUserLogin.exp < currentTime) {
    store.dispatch(logout());
    window.location.href = '/';
  }
}
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <Route exact path='/' component={Awal} />
            <Route exact path='/Masuk' component={Login} />
            <Route exact path='/Daftar' component={Daftar} />
            <Switch>
             
              <SecuredRoute exact path='/admin' component={HomeAdmin} />
              <SecuredRoute exact path='/bukuadmin' component={TampilBuku} />
              <SecuredRoute exact path='/tambahdatabuku' component={TambahBuku}
              />
              <SecuredRoute exact path='/detailbuku' component={DetailBuku} />
              <SecuredRoute exact path='/editbuku' component={EditBuku} />
              <SecuredRoute exact path='/pengguna' component={Pengguna} />
              <SecuredRoute exact path='/tambahadmin' component={TambahAdmin} />
              <SecuredRoute exact path='/detail-pengguna' component={DetailPengguna}/>
              <SecuredRoute path='/editpengguna' component={EditPengguna}/>
              <SecuredRoute exact path='/profil' component={Profil}/>
              <SecuredRoute exact path='/editprofil' component={EditProfil} />
              <SecuredRoute exact path='/ubahpassword' component={UbahPassword} />
              <SecuredRoute exact path='/ubah-password' component={DefaultPassword} />
              <SecuredRoute exact path='/laporan' component = {Laporan}/>
              <SecuredRoute exact path='/pengembalian' component={Pengembalian}/>

              <SecuredRoute
                exact
                path='/pengunjung'
                component={PagePengunjung}
              />
              <SecuredRoute exact path='/bukuP' component={TampilBukuP} />
              <SecuredRoute exact path='/detailbukup' component={DetailBukuP} />
              <SecuredRoute exact path='/keranjang' component={Keranjang}/>
              <SecuredRoute exact path='/profilp' component={ProfilP} />
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
