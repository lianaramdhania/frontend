import axios from 'axios';
import { SET_CURRENT_USER } from './types';
import setJWTToken from '../securityUtils/setJWTToken';
import jwt_decode from 'jwt-decode';

export const login = (LoginRequest) => async (dispatch) => {
  try {
    const res = await axios.post('http://localhost:8080/auth', LoginRequest);
    const { token } = res.data;

    localStorage.setItem('tokenUserLogin', token);

    if (localStorage.getItem('tokenUserLogin') === null) {
      localStorage.removeItem('tokenUserLogin');
      // if (this.state.store = action.payload) {
      alert('INVALID USERNAME OR PASSWORD');
    } else {
      setJWTToken(token);
      const decoded = jwt_decode(token);
      dispatch({
        type: SET_CURRENT_USER,
        payload: decoded,
      });
    }
  } catch (err) {
    alert('INVALID USERNAME OR PASSWORD');
  }
};

export const logout = () => (dispatch) => {
  localStorage.removeItem('tokenUserLogin');
  sessionStorage.clear();
  setJWTToken(false);
  dispatch({
    type: SET_CURRENT_USER,
    payload: {},
  });
};
