import Axios from 'axios';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './DetailBuku.css';

export class DetailBuku extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambilId : JSON.parse(localStorage.getItem("id")),
      idBuku: '',
      judulBuku: '',
      namaPengarang: '',
      jenisBuku:'',
      keterangan:'',
      hargaSewa: '',
      jumlahBuku: '',
      datasBuku: [],
    };
  }

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getDataProdukById/' +  this.state.ambilId,
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        const hasil = res.data;
        this.setState({
          datasBuku: hasil,
        });
      })
      .catch(console.error);
  }

  getId = (idBuku) => {
    const id = idBuku;
    localStorage.setItem('id', idBuku);
    console.log(id);
  };

  edits = (idBuku) => {
    console.log(idBuku);
    localStorage.setItem('id', idBuku);
    window.location.href = '/editBuku';
  }

  render() {
    return (
      <div>
        <HeaderAdmin />
        <div className='grid-detailbuku-adm'>
          <div className='detailbuku-adm-kiri'>

          </div>

          {this.state.datasBuku.map((buku, i) => (
          <div className='detailbuku-adm-kanan'>
            <h4>Detail Buku</h4>
            <p className='judul-detailbuku'>ID Buku</p>
            <p className='keterangan-detailbuku'>{buku.idBuku}</p>

            <p className='judul-detailbuku'>Judul Buku</p>
            <p className='keterangan-detailbuku'>{buku.judulBuku}</p>
             
            <p className='judul-detailbuku'>Nama Pengarang</p>
            <p className='keterangan-detailbuku'>{buku.namaPengarang}</p>

            <p className='judul-detailbuku'>Jenis Buku</p>
            <p className='keterangan-detailbuku'>{buku.jenisBuku}</p>
             
            <p className='judul-detailbuku'>Keterangan</p>
            <p className='keterangan-detailbuku'>{buku.keterangan}</p>
             
            <p className='judul-detailbuku'>Harga Sewa</p>
            <p className='keterangan-detailbuku'>{buku.hargaSewa}</p>
            
            <p className='judul-detailbuku'>Stok</p>
            <p className='keterangan-detailbuku'>{buku.jumlahBuku}</p>

            <button onClick={() => this.edits(buku.id)}>Edit</button>
            <button>Hapus</button>
          </div>
            ))}
        </div>
       </div>
    );
  }
}

export default DetailBuku;
