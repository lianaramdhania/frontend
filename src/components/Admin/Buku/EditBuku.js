import Axios from 'axios';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './EditBuku.css';

export class EditBuku extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambilId: JSON.parse(localStorage.getItem('id')),
      idBuku: '',
      judulBuku: '',
      namaPengarang: '',
      jenisBuku: '',
      keterangan: '',
      hargaSewa: '',
      jumlahBuku: '',
    };
  }

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getDataProdukById/' + this.state.ambilId,
      headers: {
        Authorization: token,
        'Content-Type': 'application/json'
      },
    })
      .then((res) => {
    
        this.setState({
          idBuku: res.data[0].idBuku,
          judulBuku: res.data[0].judulBuku,
          namaPengarang: res.data[0].namaPengarang,
          jenisBuku: res.data[0].jenisBuku,
          keterangan: res.data[0].keterangan,
          hargaSewa: res.data[0].hargaSewa,
          jumlahBuku: res.data[0].jumlahBuku,
        });
        console.log( this.state.ambilId);
        console.log(res.data.idBuku);

      })
      .catch((error) => {
        console.log(error);
      });
  }

  handlerSubmit = async (e) => {
    e.preventDefault();
    await Axios.put(
      'http://localhost:8080/updateProduk/' + this.state.ambilId,
      this.state
    );
    this.props.history.push('/bukuadmin');
  };

  handleChange = e => {
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  render() {
    const {
      idBuku,
      judulBuku,
      namaPengarang,
      jenisBuku,
      keterangan,
      hargaSewa,
      jumlahBuku,
    } = this.state;
    
    console.log('hsdjks', this.state.namaPengarang);
    return (
      <div className='semua-editbuku'>
        <HeaderAdmin />
        <h2 className='judul-editbuku'>Edit Data Buku</h2>

        <div
          className='grid-container-editbuku'
          onSubmit={this.handlerSubmit}
        >
          <div className='bagian-kiri-editbuku'>
            <label>ID Buku</label>
            <br />
            <input
              type='text'
              name='idBukuH'
              style={{ width: '70%' }}
              onChange={this.handleChange}
              value={idBuku}
              disabled
            />

            <br />

            <label>Judul Buku</label>
            <br />
            <input
              name='judulBuku'
              type='text'
              onChange={this.handleChange}
              value={judulBuku}
            ></input>
            <br />

            <label>Nama Pengarang</label>
            <br />
            <input
              name='namaPengarang'
              type='text'
              onChange={this.handleChange}
              value={namaPengarang}
            />
            <br />

            <label>Jenis Buku</label>
            <br />
            <select
              name='jenisBuku'
              onChange={this.handleChange}
              value={jenisBuku}
              disabled
            >
              <option hidden>PILIH JENIS BUKU</option>
              <option value='Novel'>Novel</option>
              <option value='Komik'>Komik</option>
              <option value='Ensiklopedia'>Ensiklopedia</option>
            </select>
            <br />
          </div>

          <div className='bagian-kanan-editbuku'>
            <label>Keterangan</label>
            <br />
            <textarea
              rows='6'
              name='keterangan'
              onChange={this.handleChange}
              value={keterangan}
            ></textarea>
            <br />

            <label>Harga Sewa</label>
            <br />
            <input
              type='number'
              name='hargaSewa'
              onChange={this.handleChange}
              value={hargaSewa}
            ></input>
            <br />

            <label>Stok</label>
            <br />
            <input
              type='number'
              name='jumlahBuku'
              onChange={this.handleChange}
              value={jumlahBuku}
            ></input>
            <br />
          </div>

          <button
            onClick={this.handlerSubmit}
            className='simpan-editbuku'
          >
            Simpan
          </button>
        </div>
      </div>
    );
  }
}
export default EditBuku;
