import Axios from 'axios';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './TambahBuku.css';

export class TambahBuku extends Component {
  state = {
    idBukuH: '',
    idBukuA: '',
    judulBuku: '',
    namaPengarang: '',
    jenisBuku: '',
    keterangan: '',
    hargaSewa: '',
    jumlahBuku: '',
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }; 

  handleButtonSubmit = () => {
    const token = "Bearer " + localStorage.getItem("tokenUserLogin")
    Axios({
      method: "POST",
      url: 'http://localhost:8080/addProduk',
      data: {
        idBuku: this.state.idBukuH + '-' + this.state.idBukuA,
        judulBuku: this.state.judulBuku,
        namaPengarang: this.state.namaPengarang,
        jenisBuku: this.state.jenisBuku,
        keterangan: this.state.keterangan,
        hargaSewa: this.state.hargaSewa,
        jumlahBuku: this.state.jumlahBuku,
      },
      headers: {
          Authorization: token
      }

    })
    .then((res) => {
      if (res.data === "BERHASIL") {
        alert(res.data);
        this.props.history.push('/admin');
      }
      else{
        alert(res.data);
      }
    });
  };

  componentDidUpdate(prevprops, prevState){
    if (prevState.jenisBuku !== this.state.jenisBuku) {
      if (this.state.jenisBuku === "Komik") {
        this.setState({
          idBukuH: "KOM"
        })
        
      }
      else if (this.state.jenisBuku === "Novel") {
        this.setState({
          idBukuH: "NOV"
        })
        
      }
      else if (this.state.jenisBuku === "Ensiklopedia") {
        this.setState({
          idBukuH: "ENS"
        })
        
      }
    }
  }

  render() {
    return (
      <div className='semua-tambahbuku'>
        <HeaderAdmin />
        <div className='konten-tambahbuku'>
        <h2 className='judul-tambahbuku'>Tambah Data Buku</h2>

        <div className='grid-container-tambahbuku'>
          <div className='bagian-kiri-tambahbuku'>
            <label>ID Buku</label>
            <br />
            <input type='text' name='idBukuH' style={{width: "20%"}} onChange={this.handleChange} value={this.state.idBukuH} disabled/>
            <input type='number' name='idBukuA' style={{width: "50%"}} onChange={this.handleChange} />
            <br />

            <label>Judul Buku</label>
            <br />
            <input name='judulBuku' type='text' onChange={this.handleChange} />
            <br />

            <label>Nama Pengarang</label>
            <br />
            <input
              name='namaPengarang'
              type='text'
              onChange={this.handleChange}
            />
            <br />

            <label>Jenis Buku</label>
            <br />
            <select name='jenisBuku' onChange={this.handleChange} value={this.state.jenisBuku}>
              <option hidden>PILIH JENIS BUKU</option>
              <option value='Novel'>Novel</option>
              <option value='Komik'>Komik</option>
              <option value='Ensiklopedia'>Ensiklopedia</option>
            </select>
            <br />
          </div>

          <div className='bagian-kanan-tambahbuku'>
            <label>Keterangan</label>
            <br />
            <textarea
              rows='5'
              name='keterangan'
              onChange={this.handleChange}
            ></textarea>
            <br />

            <label>Harga Sewa</label>
            <br />
            <input
              type='number'
              name='hargaSewa'
              onChange={this.handleChange}
            ></input>
            <br />

            <label>Stok</label>
            <br />
            <input
              type='number'
              name='jumlahBuku'
              onChange={this.handleChange}
            ></input>
            <br />
          </div>

          <button
            onClick={this.handleButtonSubmit}
            className='simpan-tambahbuku'
          >
            Simpan
          </button>
        </div>
      </div>
      </div>
    );
  }
}

export default TambahBuku;
