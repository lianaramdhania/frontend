import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './HeaderAdmin.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export class HeaderAdmin extends Component {
  render() {
    const handleButtonLogout = () => {
      let log = window.confirm('keluar ?')
      if (log) {
          localStorage.removeItem('tokenUserLogin')
          sessionStorage.clear()
          window.location.href = '/'
      }
  }
    return (
      <div>
        <div className='header-admin'>
          <Link to = '/admin'>
          <h3 className='logo'>Hi, Book!</h3>
          </Link>

          <div className='header-admin-kanan'>
            <Link to='/bukuadmin'>
  Buku
            </Link>

            <Link to='/pengguna'>
     Pengguna
            </Link>

            <Link to='/pengembalian'>
Pengembalian
            </Link>

            <Link to='/laporan'>
      Laporan
            </Link>

            <Link to='/profil'>
            <FontAwesomeIcon icon='user'/>

            </Link>

            <FontAwesomeIcon icon='sign-out-alt' onClick={handleButtonLogout}>
              <Link to='/'></Link>
            </FontAwesomeIcon>
      

          </div>
        </div>
      </div>
    );
  }
}

export default HeaderAdmin;
