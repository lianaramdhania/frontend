import React, { Component } from 'react';
import HeaderAdmin from './Header/HeaderAdmin';
import './HomeAdmin.css';
import home from '../../assets/home-adm.png';
import Axios from 'axios';
import { masa } from 'masa';
import moment from 'moment';

export class HomeAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tanggalHariIni: moment().format('yyyy-MM-DD'),
    };
  }

  handlerSearch = (e) => {
    this.setState({
      tanggalHariIni: e.target.value,
    });
  };

  componentDidMount = async () => {
    console.log(this.state.tanggalHariIni);
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');

    await Axios.get(
      `http://localhost:8080/getPenghasilan/${this.state.tanggalHariIni}`,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      }
    ).then((res) => {
      const hasil = res.data;
      this.setState({ hasil });
    });
    await Axios.get(
      `http://localhost:8080/getTerpinjam/${this.state.tanggalHariIni}`
    ).then((res2) => {
      const terpinjam = res2.data;
      this.setState({ terpinjam });
    });
    await Axios.get(
      `http://localhost:8080/getDikembalikkan/${this.state.tanggalHariIni}`
    ).then((res3) => {
      const kembali = res3.data;
      this.setState({ kembali });
    });
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (prevState.tanggalHariIni !== this.state.tanggalHariIni) {
      const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');

      await Axios.get(
        `http://localhost:8080/getPenghasilan/${this.state.tanggalHariIni}`,
        {
          headers: {
            Authorization: token,
            'Content-Type': 'application/json',
          },
        }
      ).then((res) => {
        const hasil = res.data;
        this.setState({ hasil });
      });
      await Axios.get(
        `http://localhost:8080/getTerpinjam/${this.state.tanggalHariIni}`
      ).then((res2) => {
        const terpinjam = res2.data;
        this.setState({ terpinjam });
      });
      await Axios.get(
        `http://localhost:8080/getDikembalikkan/${this.state.tanggalHariIni}`
      ).then((res3) => {
        const kembali = res3.data;
        this.setState({ kembali });
      });
    }
  };

  render() {
    console.log(this.state.tanggalHariIni);
    return (
      <div>
        <HeaderAdmin />
        <div className='semua-home-adm'>
          <div className='filter-home-adm'>
            <h6>Rentang Waktu</h6>
            <input
              type='date'
              name='tanggalHariIni'
              onChange={this.handlerSearch}
              defaultValue={moment().format('yyyy-MM-DD')}
            ></input>
          </div>
          <div className='home-admin-1'>
            <div className='box-home-adm'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Terpinjam: {this.state.terpinjam} Buku</h4>
            </div>

            <div className='box-home-adm'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Dikembalikan: {this.state.kembali} Buku</h4>
            </div>
          </div>

          <div className='home-admin-2'>
            <div className='box-home-adm'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Pendapatan: Rp.{this.state.hasil}</h4>
            </div>

            <img src={home} className='imageContent' alt='home' />
          </div>
        </div>
      </div>
    );
  }
}

export default HomeAdmin;
