import Axios from 'axios';
import { MDBDataTable } from 'mdbreact';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './Laporan.css'

export class Laporan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isLoading: true,
      tableRows: [],
    };
  }

  componentDidMount = async () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    await Axios.get(`http://localhost:8080/getLaporanByStatusKembali`, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        const posts = response.data;
        this.setState({ posts });
        console.log(posts);
      })
      //   .then((data) => {
      //     this.setState({ posts: data });
      //     console.log(data);
      //   })

      .then(async () => {
        this.setState({ tableRows: this.assemblePosts(), isLoading: false });
      });
  };

  assemblePosts = () => {
    let posts = this.state.posts.map((post) => {
      console.log(post.pinjam);
      return {
        nama: post.pengguna.nama,
        tanggalPinjam: post.tanggalPinjam,
        tanggalKembali: post.laporan.tanggalKembali,
        biaya: post.biaya,
        denda: post.laporan.denda,
        total: post.laporan.total
      };
    });
    return posts;
  };

  render() {
    const data = {
      columns: [
        {
          label: 'Nama Peminjam',
          field: 'nama',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Jumlah Buku',
          field: '',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Tanggal Pinjam',
          field: 'tanggalPinjam',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Tanggal Kembali',
          field: 'tanggalKembali',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Biaya',
          field: 'biaya',
          width: '100',
        },
        {
          label: 'Denda',
          field: 'denda',
          width: '100',
          sort: 'disabled',
        },
        {
            label: 'Total',
            field: 'total',
            width: '100',
            sort: 'disabled',
          },
      ],
      rows: this.state.tableRows,
    };
    return (
      <div className='halaman-laporan'>
        <HeaderAdmin />
        <div className='konten-tabel-laporan'>
        <h5>Tabel Laporan</h5>
          <MDBDataTable
            hover
            entriesOptions={[5, 15, 25, 50, 75, 100]}
            entries={100}
            scrollY
            maxHeight='440px'
            data={data}
            striped
            bordered
          />
        </div>
      </div>
    );
  }
}

export default Laporan;
