import Axios from 'axios';
import { MDBDataTable } from 'mdbreact';
import React, { Component } from 'react';
import swal from 'sweetalert';
import HeaderAdmin from '../Header/HeaderAdmin';
import './Pengembalian.css';

export class Pengembalian extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isLoading: true,
      tableRows: [],
    };
  }

  componentDidMount = async () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    await Axios.get(`http://localhost:8080/getPengembalian`, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        const posts = response.data;
        this.setState({ posts });
        console.log(posts);
      })
      //   .then((data) => {
      //     this.setState({ posts: data });
      //     console.log(data);
      //   })

      .then(() => {
        this.setState({ tableRows: this.assemblePosts(), isLoading: false });
      });
  };

  pengembalian = (id_pengguna) => {
    Axios.put(`http://localhost:8080/pengembalian/${id_pengguna}`, null, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('tokenUserLogin')}`,
      },
    })
      .then((res) => {
        swal({
          text: 'Buku berhasil dikembalikan',
          icon: 'success',
        });
      })
      .then((err) => {
        swal({
          text: 'Buku gagal dikembalikan',
          icon: 'error',
        });
      });

    // Axios.post(`http://localhost:8080/addLaporan/${id_pengguna}`, null, {
    //   headers: {
    //     Authorization: `Bearer ${localStorage.getItem('tokenUserLogin')}`,
    //   },
    // })
    // .then((res) => {
    //   swal({
    //     text: 'Buku berhasil dikembalikan',
    //     icon: 'success',
    //   });
    // })
    // .then((err) => {
    //   swal({
    //     text: 'Buku gagal dikembalikan',
    //     icon: 'error',
    //   });
    // });
  };

  assemblePosts = () => {
    let posts = [];
    console.log(posts);
    this.state.posts.forEach((post) => {
      const currentPost = posts.findIndex(
        (p) => p.pengguna.id_pengguna === post.pengguna.id_pengguna
      );
      // console.log(post.pengguna.id_pengguna);

      if (currentPost < 0) {
        posts.push(post);
      }
    });

    return posts.map((post) => ({
      nama: post.pengguna.nama,
      tanggalPinjam: post.tanggalPinjam,
      biaya: post.biaya,
      aksi: (
        <div>
          <button
            className='tombol-pengembalian-admin'
            onClick={() => this.pengembalian(post.pengguna.id_pengguna)}
          >
            Dikembalikan
          </button>
        </div>
      ),
    }));
  };

  render() {
    const data = {
      columns: [
        {
          label: 'Nama',
          field: 'nama',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Tanggal Pinjam',
          field: 'tanggalPinjam',
          width: '100',
          sort: 'disabled',
        },
        {
          label: 'Biaya',
          field: 'biaya',
          width: '100',
        },
        {
          label: 'Aksi',
          field: 'aksi',
          width: '100',
          sort: 'disabled',
        },
      ],
      rows: this.state.tableRows,
    };
    return (
      <div className='halaman-pengembalian'>
        <HeaderAdmin />
        <div className='konten-tabel-pengembalian'>
          <h5>Tabel Pengembalian</h5>
          <MDBDataTable
            hover
            entriesOptions={[5, 15, 25, 50, 75, 100]}
            entries={100}
            scrollY
            maxHeight='440px'
            data={data}
            striped
            bordered
          />
        </div>
      </div>
    );
  }
}

export default Pengembalian;
