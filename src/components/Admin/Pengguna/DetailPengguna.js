import Axios from 'axios';
import React, { Component, Fragment } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './DetailPengguna.css';
import masuk from '../../../assets/login.png';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

export class DetailPengguna extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambilId: JSON.parse(localStorage.getItem('id_pengguna')),
      users: [],
      token: '',
      setelPass: '12345',
    };
  }

  componentDidMount = () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    const { user } = this.props.security;
    // console.log(user.id_pengguna);
    Axios.get(
      `http://localhost:8080/getDataPenggunaById/` + this.state.ambilId,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      }
    ).then((res) => {
      const users = res.data;
      this.setState({
        users: users,
      });
      console.log(res.data);
    });
  };

  edits = (id_pengguna) => {
    console.log(id_pengguna);
    localStorage.setItem('id_pengguna', id_pengguna);
    this.props.history.push('/editpengguna');
  };

  handlerSubmit = () => {
  Axios({
      method: 'PUT',
          url: `http://localhost:8080/ubahPassword/` + this.state.ambilId,
          headers:{},
          data: {
              password: this.state.setelPass
          },
        }).then((res) => {
            console.log(res);
            console.log(res.data);
            alert('Reset Password Berhasil');
            this.props.history.push('/pengguna');

          });
        };


  render() {
    return (
      <Fragment>
        <HeaderAdmin />
        <div className='halaman-detail-pengguna'>
          {this.state.users.map((user, idx) => (
            <div className='card-detail-pengguna'>
              <div className='card-image-detail'>
                <img src={masuk} className='imageContent' alt='login' />
              </div>
              <div class='card-nama-detail-pengguna'>
                <h4>
                  {user.nama} ({user.username})
                </h4>

                <p>{user.email}</p>
                <p>{user.nomorHp}</p>
                <p>{user.status}</p>
                <div className='alamat-detail-pengguna'>
                  <p>{user.alamat}</p>
                </div>
                <button
                  className='ubah-detail-pengguna'
                  onClick={() => this.edits(user.id_pengguna)}
                >
                  EDIT DATA
                </button>
                <button
                  className='reset-password-detail'
                  onClick={this.handlerSubmit}
                >
                  RESET PASSWORD
                </button>
              </div>
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

DetailPengguna.propTypes = {
  security: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(DetailPengguna);
