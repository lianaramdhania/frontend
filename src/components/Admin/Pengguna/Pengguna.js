import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './Pengguna.css';
import HeaderAdmin from '../Header/HeaderAdmin';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { Link } from 'react-router-dom';
import Axios from 'axios';

class Pengguna extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambilId: JSON.parse(localStorage.getItem('id_pengguna')),
      datasPengguna: [],
      posts: [],
      isLoading: true,
      tableRows: [],
    };
  }

  componentDidMount = async () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    await axios
      .get(`http://localhost:8080/getPengguna`, {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      })
      .then((response) => response.data)
      .then((data) => {
        this.setState({ posts: data });
      })
      .then(async () => {
        this.setState({ tableRows: this.assemblePosts(), isLoading: false });
      });
  };

  assemblePosts = () => {
    let posts = this.state.posts.map((post) => {
      return {
        id_pengguna: post.id_pengguna,
        username: post.username,
        nama: post.nama,
        alamat: post.alamat,
        nomorHp: post.nomorHp,
        email: post.email,
        tanggalRegis: post.tanggalRegis,
        status: post.status,
        aksi: [
          <div>
            <button
              className='tombol-detailpengguna-admin'
              onClick={() => this.details(post.id_pengguna)}
            >
              DETAIL
            </button>
          </div>,
        ],
      };
    });
    return posts;
  };

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getDataPenggunaById/' + this.state.ambilId,
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        const hasil = res.data;
        this.setState({
          datasPengguna: hasil,
        });
      })
      .catch(console.error);
  }

  details = (id_pengguna) => {
    console.log(id_pengguna);
    localStorage.setItem('id_pengguna', id_pengguna);
    window.location.href = '/detail-pengguna';
  };

  render() {
    const data = {
      columns: [
        {
          label: 'ID Pengguna',
          field: 'id_pengguna',
          width: '70',
          sort: 'disabled',
        },
        {
          label: 'Nama',
          field: 'nama',
          width: '120',
        },
        {
          label: 'E-Mail',
          field: 'email',
          width: '120',
          sort: 'disabled',
        },
        {
          label: 'Status',
          field: 'status',
          width: '120',
        },
        {
          label: 'Aksi',
          field: 'aksi',
          width: '120',
          sort: 'disabled',
        },
      ],
      rows: this.state.tableRows,
    };
    return (
      <div className='halaman-pengguna'>
        <HeaderAdmin />
        <Link to='/tambahadmin'>
          <button className='tombol-tambahdata-admin'>Tambah Admin</button>
        </Link>
        <div className='konten-tabel-pengguna'>
          <MDBDataTable
            hover
            entriesOptions={[5, 15, 25, 50, 75, 100]}
            entries={100}
            scrollY
            maxHeight='440px'
            data={data}
            striped
            bordered
          />
        </div>
      </div>
    );
  }
}

Pengguna.propTypes = {
  security: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(Pengguna);
