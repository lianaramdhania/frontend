import Axios from 'axios';
import { masa } from 'masa';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import './TambahAdmin.css';

const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
// const regexPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,8}$/;
const regexNomor = /^08[0-9]{9,}$/;
const regexUsername = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;

export class TambahAdmin extends Component {
  state = {
    nama: '',
    alamat: '',
    email: '',
    nomorHp: '',
    status: 'Admin',
    username: '',
    password: 'Admin123',
    tanggalRegis: masa().format('DD-MM-YYYY'),
    namaError: '',
    alamatError: '',
    emailError: '',
    nomorError: '',
    usernameError: '',
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  validate = () => {
    let namaError = '';
    let alamatError = '';
    let emailError ='';
    let nomorError = '';
    let usernameError = '';

    if (!this.state.nama) {
      namaError = 'NAMA TIDAK BOLEH KOSONG';
    }

    if (!this.state.alamat) {
      alamatError = 'ALAMAT TIDAK BOLEH KOSONG';
    }

    if (!this.state.email.match(regexEmail)) {
      emailError = 'EMAIL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.nomorHp.match(regexNomor)) {
      nomorError = 'NOMOR PONSEL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.username) {
      usernameError = 'USERNAME HARUS MEMILIKI MIN 6 KARAKTER DAN TIDAK MENGGUNAKAN SIMBOL';
    }

  
    if (namaError || alamatError || emailError || nomorError || usernameError ) {
      this.setState({ namaError, alamatError, emailError, nomorError, usernameError });
      return false;
    }

    return true;
  };

  handleButtonSubmit = () => {
    const isValid = this.validate();
    if (isValid) {
      Axios({
        method: 'POST',
        url: `http://localhost:8080/public/addPengguna`,
        headers: {},
        data: {
          nama: this.state.nama,
          username: this.state.username,
          password: this.state.password,
          email: this.state.email,
          status: this.state.status,
          alamat: this.state.alamat,
          nomorHp: this.state.nomorHp,
          tanggalRegis: this.state.tanggalRegis,
        },
      }).then((res) => {
        console.log(res);
        console.log(res.data);
        alert('Register Berhasil' + this.state.nama);
        window.location.href = '/pengguna';
      });
    }
  };

  render() {
    return (
      <div className='semua-tambahadmin'>
        <HeaderAdmin />
        <div className='konten-tambahadmin'>
        <h2 className='judul-tambahadmin'>Tambah Data Admin</h2>

        <div className='grid-container-tambahadmin'>
          <div className='bagian-kiri-tambahadmin'>
            <label>Nama</label>
            <br />
            <input type='text' name='nama' onChange={this.handleChange} />
            <div className='validasi'>{this.state.namaError}</div>
            <br />

            <label>Nomor Telepon</label>
            <br />
            <input name='nomorHp' type='text' onChange={this.handleChange} />
            <div className='validasi'>{this.state.nomorError}</div>

            <br />

            <label>E-Mail</label>
            <br />
            <input name='email' type='text' onChange={this.handleChange} />
            <div className='validasi'>{this.state.emailError}</div>

            <br />
          </div>

          <div className='bagian-kanan-tambahadmin'>
            <label>Alamat</label>
            <br />
            <textarea
              name='alamat'
              rows='6'
              onChange={this.handleChange}
            ></textarea>
            <div className='validasi'>{this.state.alamatError}</div>

            <br />

            <label>Username</label>
            <br />
            <input
              name='username'
              type='text'
              onChange={this.handleChange}
            ></input>
            <div className='validasi'>{this.state.usernameError}</div>
            <br />
          </div>
        </div>
        <button
          onClick={this.handleButtonSubmit}
          className='simpan-tambahadmin'
        >
          Simpan
        </button>
      </div>
      </div>
    );
  }
}

export default TambahAdmin;
