import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import HomeAdmin from './HomeAdmin';
import TampilBuku from './Buku/TampilBuku';
import Pengguna from './Pengguna/Pengguna';
import Profil from './Profil/Profil';
import UbahPassword from './Profil/UbahPassword';
import DetailPengguna from './Pengguna/DetailPengguna';
import Laporan from './Laporan/Laporan';
import DefaultPassword from '../DefaultPassword';

class PageAdmin extends Component {
  render() {
    // const { validToken, user } = this.props.security;
    const { user } = this.props.security;
    console.log(user.password);
    if (user.password === '12345') {
      return <Redirect to='/ubah-password' />;
    }

    if (user.status !== 'Admin') {
      return <Redirect to='/pengunjung' />;
    }

    return (
      <>
        <div>
          <Router>
            <Switch>
              <Route exact path='/admin' component={HomeAdmin} />
              <Route path='/bukuadmin' component={TampilBuku} />
              <Route path='/pengguna' component={Pengguna} />
              <Route path='/laporan' component={Laporan} />
              <Route path='/profil' component={Profil} />
              <Route path='/ubahpassword' component={UbahPassword} />
              <Route path='/ubah-password' component={DefaultPassword} />

              <Route path='/detail-pengguna' component={DetailPengguna} />
            </Switch>
          </Router>
        </div>
      </>
    );
  }
}

PageAdmin.propTypes = {
  security: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(PageAdmin);
