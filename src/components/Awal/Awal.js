import React, { Component } from 'react';
import './Awal.css';
import awal from '../../assets/awal.png';
import { Link } from 'react-router-dom';

export class Awal extends Component {
  render() {
    return (
      <div>
        <div className='header-awal'>
          <a className='logo'>Hi, Book!</a>
          <div className='header-awal-kanan'>
            <Link to='/masuk'>
              <a>Masuk</a>
            </Link>
            <Link to='/daftar'>
              <a>Daftar</a>
            </Link>

          </div>
        </div>

        <div class='grid-container'>
          <div className='container-kiri'>
            <p className='hai'>Hai, ini Hi-Book!</p>
            <p className='slogan'>
              Pengen baca buku bagus, <br /> tapi dompet lagi kurus?
              <br />
              Yuk pinjam di Hi-Read, <br /> dijamin hemat!
            </p>
          </div>

          <div className='container-kanan'>
            <img src={awal} className='awalImage' alt='awal' />
          </div>
        </div>
      </div>
    );
  }
}

export default Awal;
