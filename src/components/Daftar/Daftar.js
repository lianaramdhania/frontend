import React, { Component } from 'react';
import './Daftar.css';
import daftar from '../../assets/register.png';
import Axios from 'axios';
import { masa } from 'masa';
import { Link } from 'react-router-dom';

const regexNama =  /^[a-zA-Z\s]{4,30}$/;
const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const regexNomor = /^08[0-9]{9,}$/;
const regexUsername = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
class Daftar extends Component {
  state = {
    nama: '',
    alamat: '',
    email: '',
    nomorHp: '',
    status: 'Teman',
    username: '',
    password: '12345',
    tanggalRegis: masa().format('DD-MM-YYYY'),
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  validate = () => {
    let namaError = '';
    let alamatError = '';
    let emailError ='';
    let nomorError = '';
    let usernameError = '';

    if (!this.state.nama.match) {
      namaError = 'NAMA TIDAK BOLEH KOSONG';
    }

    if (!this.state.alamat) {
      alamatError = 'ALAMAT TIDAK BOLEH KOSONG';
    }

    if (!this.state.email.match(regexEmail)) {
      emailError = 'EMAIL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.nomorHp.match(regexNomor)) {
      nomorError = 'NOMOR PONSEL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.username) {
      usernameError = 'USERNAME HARUS MEMILIKI MIN 6 KARAKTER DAN TIDAK MENGGUNAKAN SIMBOL';
    }

  
    if (namaError || alamatError || emailError || nomorError || usernameError ) {
      this.setState({ namaError, alamatError, emailError, nomorError, usernameError });
      return false;
    }

    return true;
  };

  handleButtonSubmit = () => {
    const isValid = this.validate();
    if (isValid) {
    Axios({
      method: 'POST',
      url: `http://localhost:8080/public/addPengguna`,
      headers: {},
      data: {
        nama: this.state.nama,
        username: this.state.username,
        password: this.state.password,
        email: this.state.email,
        status: this.state.status,
        alamat: this.state.alamat,
        nomorHp: this.state.nomorHp,
        tanggalRegis: this.state.tanggalRegis,
      },
    }).then((res) => {
      console.log(res);
      console.log(res.data);
      alert('Register Berhasil, ' + this.state.nama);
      this.props.history.push('/masuk');
    });
  }
};
  render() {
    return (
      <div>
        <div className='header'>
          <div className='logo'>Hi, Book!</div>
          <div className='header-right'>
            <Link to='/masuk'>
              <a>Masuk</a>
            </Link>
            
          </div>
        </div>

        <div className='grid-container'>
          <div className='bagian-kiri-daftar'>
            <img src={daftar} alt='daftar' />
          </div>

          <div className='bagian-kanan-daftar'>
            <p>
              Belum punya akun, ya?
              <br />
              Yuk Daftar dan jadi bagian dari Hi-Book!
            </p>
            <label>Nama Lengkap</label>
            <br />

            <input type='text' name='nama' onChange={this.handleChange}></input>
            <br />

            <label>Nomor Telepon</label>
            <br />
            <input
              type='text'
              name='nomorHp'
              onChange={this.handleChange}
            ></input>
            <div className='validasi'>{this.state.nomorError}</div>
            <br />

            <label>E-Mail</label>
            <br />
            <input
              type='text'
              name='email'
              onChange={this.handleChange}
            ></input>
            <div className='validasi'>{this.state.emailError}</div>
            <br />

            <label>Alamat</label>
            <br />
            <input
              type='text'
              name='alamat'
              onChange={this.handleChange}
            ></input>
            <div className='validasi'>{this.state.alamatError}</div>
            <br />

            <label>Username</label>
            <br />
            <input
              type='text'
              name='username'
              onChange={this.handleChange}
            ></input>
            <div className='validasi'>{this.state.usernameError}</div>
            <br />
            <button
              className='tombol-submit-daftar'
              onClick={this.handleButtonSubmit}
            >
              Daftar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Daftar;
