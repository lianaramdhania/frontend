import Axios from 'axios';
import React, { Component } from 'react';

const regexPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,8}$/;
export class DefaultPassword extends Component {
    state = {
        status: '',
        ambilId: JSON.parse(localStorage.getItem('id_pengguna')),
        password: '',
        konfirmasiPass: '',
        passwordError: '',
        konfirmasiPassError: '',
      };
    
      componentDidMount() {
        const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
        Axios({
          method: 'GET',
          url: 'http://localhost:8080/getDataPenggunaById/' + this.state.ambilId,
          headers: {
            Authorization: token,
            'Content-Type': 'application/json',
          },
        })
      }
    
      // BATAS SUCI
      handlerSubmit = async () => {
            await Axios.put(
              'http://localhost:8080/ubahPassword/' + this.state.ambilId,
              this.state
            );
            // this.props.history.push('/profil');
            let log = window.confirm('Yakin Ingin Mengubah Password ?')
            if (log) {
                localStorage.removeItem('tokenUserLogin')
                sessionStorage.clear()
                window.location.href = '/'
          }
        }

    
      handleChange = (e) => {
        this.setState({
          [e.target.name]: e.target.value,
        });
      };
    
      konfirmasi = () => {
        let passwordError = '';
        let konfirmasiPassError = '';
    
        if (!this.state.password.match(regexPass)) {
          passwordError = 'PASSWORD HARUS MEMILIKI SATU HURUF BESAR DAN ANGKA';
        }
    
        if (this.state.password !== this.state.konfirmasiPass) {
          konfirmasiPassError = 'PASSWORD TIDAK SAMA';
        }
    
        if (passwordError || konfirmasiPassError) {
          this.setState({
            passwordError,
            konfirmasiPassError,
          });
          return false;
        }
        return true;
      };
    
      render() {
        return (
          <div>
            <div className='card-ubahpass-adm' style={{ marginTop: 80 }}>
              <h3>UBAH PASSWORD</h3>
              <br/>
    
              <label>Password Baru</label>
              <br />
              <input type='password' name='password' onChange={this.handleChange}  minLength='6'
                maxLength='8'/>
              <div className='validasi'>{this.state.passwordError}</div>  
    
              <label>Konfirmasi Password Baru</label>
              <br />
              <input
                type='password'
                name='konfirmasiPass'
                onChange={this.handleChange}
                minLength='6'
                maxLength='8'
              />
              <div className='validasi'>{this.state.konfirmasiPassError}</div>
    
              <br />
              <button onClick={this.handlerSubmit}>Simpan</button>
            </div>
          </div>
        );
      }
    }

export default DefaultPassword;
