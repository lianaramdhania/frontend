import { library } from '@fortawesome/fontawesome-svg-core'
import { faSignOutAlt , faUser , faFileInvoice, faHome, faBook, faUsers, faCartPlus, faTrashAlt} from '@fortawesome/free-solid-svg-icons'
 
library.add(faSignOutAlt, faUsers , faUser, faFileInvoice, faHome, faBook, faCartPlus, faTrashAlt)