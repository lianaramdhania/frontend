import React, { Component } from 'react';
import './Login.css';
import masuk from '../../assets/login.png';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../actions/securityActions';
import { Link, Redirect } from 'react-router-dom';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      errors: {},
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleButtonSubmit = this.handleButtonSubmit.bind(this);
  }

  // componentDidMount() {
  //   if (this.props.security.validToken) {
  //     this.props.history.push('/admin');
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    const { user } = nextProps.security;

    if (nextProps.security.validToken) {
      if (user.password === '12345') {
        return this.props.history.push('/ubah-password');
      }
      if (user.status === 'Admin') {
        alert('hai');
        this.props.history.push('/admin');
      } else if (user.status === 'Teman' || user.status === 'Sahabat') {
        alert('halo');
        this.props.history.push('/pengunjung');
      } else {
        alert('Status tidak dikenali');
      }
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleButtonSubmit = (e) => {
    e.preventDefault();
    const LoginRequest = {
      username: this.state.username,
      password: this.state.password,
    };
    this.props.login(LoginRequest);
  };

  render() {
    console.log(this.props.login);
    return (
      <div>
        <div className='Mgrid-container'>
          <div className='Ma'>Hi, Book!</div>

          <div className='Mb'>
            <Link to='/daftar'>
              <button className='tombol-daftar'>Daftar</button>
            </Link>
          </div>

          <div className='Mc'>
            <img src={masuk} alt='login' />
          </div>

          <div className='Md'>
            <p className='Mteks'>Selamat Datang!</p>
            <p className='Mteks2'>Silahkan Masuk ke Akun Anda</p>
            <div className='Lform'>
              <label>Username</label>
              <br />
              <input
                type='text'
                name='username'
                onChange={this.handleChange}
              ></input>
              <br />
              <label>Password</label>
              <br />
              <input
                type='text'
                name='password'
                onChange={this.handleChange}
              ></input>
              <br />
            </div>
            {/* <label>Lupa Password</label><br/> */}
            <button
              className='tombol-submit-login'
              onClick={this.handleButtonSubmit}
            >
              Masuk
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  security: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  console.log(state);
  return {
    security: state.security,
    errors: state.errors,
  };
};

export default connect(mapStateToProps, { login })(Login);
