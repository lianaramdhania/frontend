import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import { masa } from 'masa';
import React, { Component } from 'react';
import swal from 'sweetalert';
import HeaderPengunjung from '../Header/HeaderPengunjung';
import './DetailBukuP.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';

export class DetailBukuP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambilId: JSON.parse(localStorage.getItem('id')),
      jumlahBuku: null,
      datasBuku: [],
      count: 0,
      token: 'Bearer ' + localStorage.getItem('tokenUserLogin'),

      //UNTUK UPLOAD KE PESANAN
      statusBuku: 'KERANJANG',
      total: 0,
      id_pengguna: JSON.parse(localStorage.getItem('id_pengguna')),
      nama: localStorage.getItem('nama'),
      username: localStorage.getItem('username'),
      password: localStorage.getItem('password'),
      alamat: localStorage.getItem('alamat'),
      id: JSON.parse(localStorage.getItem('id')),
      judulBuku: localStorage.getItem('judulBuku'),
      namaPengarang: localStorage.getItem('namaPengarang'),
      jenisBuku: localStorage.getItem('jenisBuku'),
      keterangan: localStorage.getItem('keterangan'),
      hargaSewa: localStorage.getItem('hargaSewa'),
      tanggalPinjam: localStorage.getItem('tanggalPinjam'),

      //UNTUK GANTI STOK
      judulBuku: null,
      namaPengarang: null,
      jenisBuku: null,
      keterangan: null,
      hargaSewa: null,
      jumlahBuku: null,
      statusBuku: null,
    };
  }

  componentDidMount() {
    console.log('>>>>>>>>>', this.state.ambilId);

    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getDataProdukById/' + this.state.ambilId,
      headers: {
        Authorization: this.state.token,
      },
    })
      .then((res) => {
        const hasil = res.data;
        this.setState({
          datasBuku: hasil,
          idBuku: res.data.idBuku,
          judulBuku: res.data.judulBuku,
          namaPengarang: res.data.namaPengarang,
          jenisBuku: res.data.jenisBuku,
          jumlahBuku: res.data.jumlahBuku,
          hargaSewa: res.data.hargaSewa,
          keterangan: res.data.keterangan,
        });
      })
      .catch(console.error);
  }

  getId = (idBuku) => {
    const id = idBuku;
    localStorage.setItem('id', idBuku);
    console.log(id);
  };

  pinjam = () => {
    const { user } = this.props.security;
    Axios({
      method: 'GET',
      url: `http://localhost:8080/getJumlahProduk/${this.state.datasBuku[0].id}/${user.id_pengguna}`,
      headers: {
        Authorization: this.state.token,
      },
    }).then((res) => {
      console.log(res);
      if (res.data === 'Buku sudah ada') {
        swal({
          title: res.data,
          icon: 'error',
        });
      } else {
        console.log('yaaa');
        console.log(this.state.id);

        Axios({
          method: 'POST',
          url: `http://localhost:8080/addPinjam/`,
          headers: {
            Authorization: this.state.token,
          },
          data: {
            statusBuku: 'keranjang',
            biaya: this.state.biaya,
            pengguna: {
              id_pengguna: user.id_pengguna,
            },
            produk: {
              id: this.state.id,
              judulBuku: this.state.judulBuku,
            },
          },
        }).then(() =>
          swal({
            title: 'SUKSES',
            icon: 'success',
          })
        );
      }
    });
  };

  render() {
    console.log(this.state.tanggalPinjam);
    return (
      <div>
        <HeaderPengunjung />
        <div className='semua-detailbuku-pengunjung'>
          <div className='grid-detailbuku-pengunjung'>
            <div className='detailbuku-pengunjung-kiri'></div>

            {this.state.datasBuku.map((buku, i) => (
              <div className='detailbuku-pengunjung-kanan' key={buku.idBuku}>
                <h4>Detail Buku</h4>
                <p className='judul-detailbuku'>ID Buku</p>
                <p className='keterangan-detailbuku'>{buku.idBuku}</p>

                <p className='judul-detailbuku'>Judul Buku</p>
                <p className='keterangan-detailbuku'>{buku.judulBuku}</p>

                <p className='judul-detailbuku'>Nama Pengarang</p>
                <p className='keterangan-detailbuku'>{buku.namaPengarang}</p>

                <p className='judul-detailbuku'>Jenis Buku</p>
                <p className='keterangan-detailbuku'>{buku.jenisBuku}</p>

                <p className='judul-detailbuku'>Keterangan</p>
                <p className='keterangan-detailbuku'>{buku.keterangan}</p>

                <p className='judul-detailbuku'>Harga Sewa</p>
                <p className='keterangan-detailbuku'>{buku.hargaSewa}</p>

                <p className='judul-detailbuku'>Stok</p>
                <p className='keterangan-detailbuku'>{buku.jumlahBuku}</p>

                <button onClick={this.pinjam}>
                  {' '}
                  <FontAwesomeIcon icon='cart-plus' />{' '}
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

DetailBukuP.propTypes = {
  security: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(DetailBukuP);
