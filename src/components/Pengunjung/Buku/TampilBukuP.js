import Axios from 'axios';
import React, { Component } from 'react';
import masuk from '../../../assets/login.png';
import { Link } from 'react-router-dom';
import './TampilBukuP.css';
import HeaderPengunjung from '../Header/HeaderPengunjung';

export class TampilBukuP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idBuku: '',
      judulBuku: '',
      namaPengarang: '',
      hargaSewa: '',
      datasBuku: [],
    };
  }

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getProduk',
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        const hasil = res.data;
        this.setState({
          datasBuku: hasil,
        });
      })
      .catch(console.error);
  }

  getId = (idBuku) => {
    const id = idBuku;
    localStorage.setItem('id', idBuku);
    console.log(id);
  };

  details = (idBuku) => {
    console.log(idBuku);
    localStorage.setItem('id', idBuku);
    window.location.href = '/detailbukup';
  };

  //DIGUNAKAN UNTUK SEARCH

  componentDidUpdate(prevProps, prevState) {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');

    if (prevState.search !== this.state.search) {
      const formData = new FormData();
      formData.append('search', this.state.search);
      Axios({
        method: 'GET',
        url: 'http://localhost:8080/searchProduk/?search=' + this.state.search,
        data: formData,
        headers: {
          Authorization: token,
        },
      }).then((res) => {
        const hasil = res.data;
        this.setState({
          datasBuku: hasil,
        });
      });
    }
  }

  hasilSearch = (e) => {
    this.setState({
      search: e.target.value,
    });

    if (this.state.datasBuku.length > 0) {
      return this.state.datasBuku.map((index, i) => (
        <div
          className='grid-container-tampilbuku-pengunjung'
          key={index.idBuku}
        >
          <div className='cardContainer-pengunjung' key={i}>
            <div
              className='card-pengunjung'
              onClick={() => this.details(index.id)}
            >
              <div className='card-image'>
                <img src={masuk} className='imageContent' alt='login' />
              </div>
              <div className='card-name-pengunjung'>
                <h4>{index.namaPengarang}</h4>
                <p className='judulbuku-pengunjung'>{index.judulBuku}</p>
                <br />
                <p>{index.hargaSewa}</p>
              </div>
            </div>
          </div>
          {/* ))} */}
        </div>
      ));
    }
  };

  render() {
    return (
      <>
        <div className='semua-tampilbuku-pengunjung'>
          <div className='konten-pencarian-buku-pengunjung sticky'>
            <input
              type='text'
              placeholder='Cari..'
              name='search'
              onKeyUp={this.hasilSearch}
            />
            <button type='submit'>
              <i className='fa fa-search'></i>
            </button>
          </div>

          <div className='grid-container-tampilbuku-pengunjung'>
            {this.state.datasBuku.map((index, i) => (
              <div className='cardContainer-pengunjung' key={index.idBuku}>
                <div
                  className='card-pengunjung'
                  onClick={() => this.details(index.id)}
                >
                  <div className='card-image'>
                    <img
                      src={masuk}
                      className='imageContent-pengunjung'
                      alt='login'
                    />
                  </div>
                  <div className='card-name-pengunjung'>
                    <h4>{index.namaPengarang}</h4>
                    <p className='judulbuku-pengunjung'>{index.judulBuku}</p>
                    <br />
                    <p>{index.hargaSewa}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <HeaderPengunjung />
      </>
    );
  }
}

export default TampilBukuP;
