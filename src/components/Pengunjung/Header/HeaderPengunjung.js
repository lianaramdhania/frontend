import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './HeaderPengunjung.css'

export class HeaderPengunjung extends Component {
  render() {
    const handleButtonLogout = () => {
      let log = window.confirm('keluar ?');
      if (log) {
        localStorage.removeItem('tokenUserLogin');
        sessionStorage.clear();
        window.location.href = '/';
      }
    };
    return (
      <div>
        <div className='header-pengunjung'>
          <Link to='/pengunjung'>
            <h3 className='logo'>Hi, Book!</h3>
          </Link>

          <div className='header-pengunjung-kanan'>
            <Link to='/bukuP'>
              Buku
            </Link>

            <Link to='/keranjang'>
              Keranjang
            </Link>

            <Link to='/profilp'>
              <FontAwesomeIcon icon='user' />
            </Link>

            <FontAwesomeIcon icon='sign-out-alt' onClick={handleButtonLogout}>
              <Link to='/'></Link>
            </FontAwesomeIcon>
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderPengunjung;
