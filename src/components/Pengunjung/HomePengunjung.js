import React, { Component } from 'react';
import { HeaderPengunjung } from './Header/HeaderPengunjung';
import './HomePengunjung.css';
import home from '../../assets/home-adm.png';
import { MDBDataTable } from 'mdbreact';
import Axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export class HomePengunjung extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      token: '',
      hasil: '',
    };
  }

  componentDidMount = async () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');

    const { user } = this.props.security;
    console.log(user);

    await Axios.get(
      `http://localhost:8080/getJumlahPinjam/` + user.id_pengguna,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      }
    ).then((res) => {
      const hasil = res.data;
      this.setState({
        hasil,
      });
    });

    await Axios.get(
      `http://localhost:8080/getJumlahDikembalikan/` + user.id_pengguna,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      }
    ).then((res2) => {
      const kembali = res2.data;
      this.setState({ kembali });
    });
    let pinjam;
    await Axios.get(
      `http://localhost:8080/getDataPinjamByIdPengguna/${user.id_pengguna}`,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      }
    ).then((res3) => {
      pinjam = res3.data;
    });
    
    // Axios.get(
    //   `http://localhost:8080/getDataLaporanById/${pinjam[0].laporan.id}`,
    //   {
    //     headers: {
    //       Authorization: token,
    //       'Content-Type': 'application/json',
    //     },
    //   }
    // );

    // await axios.post(`http://localhost:8282/pesananSehari/${this.state.tanggalHariIni}`).then((res3) => {
    // 	const kursi = res3.data;
    // 	this.setState({ kursi });
    // });
  };

  render() {
    // const data = {
    //   columns: [
    //     {
    //       label: 'Judul Buku',
    //       field: 'judul_buku',
    //       width: '70',
    //       sort: 'disabled',
    //     },
    //     {
    //       label: 'Nama',
    //       field: 'nama',
    //       width: '120',
    //     },
    //     {
    //       label: 'E-Mail',
    //       field: 'email',
    //       width: '120',
    //       sort: 'disabled',
    //     },
    //     {
    //       label: 'Status',
    //       field: 'status',
    //       width: '120',
    //     },
    //     {
    //       label: 'Aksi',
    //       field: 'aksi',
    //       width: '120',
    //       sort: 'disabled',
    //     },
    //   ],
    //   rows: this.state.tableRows,
    // };
    return (
      <div>
        <HeaderPengunjung />
        <div className='semua-home-pengunjung'>
          <div className='home-pengunjung-1'>
            <div className='box-home-pengunjung'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Telah Dipinjam: {this.state.kembali} Buku</h4>
            </div>

            <div className='box-home-pengunjung'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Biaya Meminjam: </h4>
            </div>
          </div>

          <div className='home-pengunjung-2'>
            <div className='box-home-pengunjung'>
              <img src={home} className='imageContent' alt='home' />
              <h4>Belum dikembalikan: {this.state.hasil} Buku</h4>
            </div>

            <img src={home} className='imageContent' alt='home' />
          </div>

          {/* <div className='konten-tabel-histori-pengunjung'>
          <MDBDataTable
            hover
            entriesOptions={[5, 15, 25, 50, 75, 100]}
            entries={100}
            scrollY
            maxHeight='440px'
            data={data}
            striped
            bordered
          />
        </div> */}
        </div>
      </div>
    );
  }
}

HomePengunjung.propTypes = {
  security: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(HomePengunjung);
