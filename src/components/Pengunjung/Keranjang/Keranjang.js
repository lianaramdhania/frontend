import Axios from 'axios';
import React, { Component } from 'react';
import HeaderPengunjung from '../Header/HeaderPengunjung';
import './Keranjang.css';
import decoder from 'jwt-decode';
import moment from 'moment';
import swal from 'sweetalert';
import masuk from '../../../assets/login.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Keranjang extends Component {
  state = {
    token: decoder(localStorage.tokenUserLogin),
    daftarBuku: [],
    biaya: '',
    // token2 : 'Bearer ' + localStorage.getItem("tokenUserLogin")
  };

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');

    try {
      Axios({
        method: 'GET',
        url:
          'http://localhost:8080/getDataPinjamByIdPenggunadanStatus/' +
          this.state.token.id_pengguna,
        headers: {
          Authorization: token,
        },
      }).then((res) => {
        const daftarBuku = res.data;
        this.setState({
          daftarBuku,
        });
      });
    } catch (error) {
      alert(error);
    }
  }

  pinjam = () => {
    const tanggalPinjam = moment().format('yyyy-MM-DD, HH:mm:ss');
    let total = 0;
    for (let i = 0; i < this.state.daftarBuku.length; i++) {
      total += this.state.daftarBuku[i].produk.hargaSewa;
    }
    const data = {
      tanggalPinjam,
      biaya: total,
    };

    // Handler peminjaman
    const novel = this.state.daftarBuku.filter((item) => {
      return item.produk.jenisBuku === 'Novel';
    });
    const komik = this.state.daftarBuku.filter((item) => {
      return item.produk.jenisBuku === 'Komik';
    });
    const ensiklopedia = this.state.daftarBuku.filter((item) => {
      return item.produk.jenisBuku === 'Ensiklopedia';
    });
    const getStatusUser = this.state.daftarBuku[0].pengguna.status;
    if (getStatusUser === 'Teman') {
      if (novel.length === 2) {
        if (komik.length > 0 || ensiklopedia.length > 0) {
          return swal({
            text: 'Anda sudah meminjam lebih dari 2 buku novel',
          });
        }
      } else if (novel.length === 1 && komik.length == 1) {
        if (ensiklopedia.length > 0) {
          return swal({
            text: 'Anda sudah meminjam lebih dari 1 novel dan 1 komik',
          });
        }
      } else if (ensiklopedia.length >= 1) {
        if (komik.length > 0 || novel.length > 0 || ensiklopedia.length > 1) {
          return swal({ text: 'Anda sudah meminjam 1 Buku Ensiklopedia' });
        }
      }

      //Sahabat
    } else {
      if (novel.length >= 4) {
        if (komik.length > 0 || ensiklopedia.length > 0 || novel.length > 4) {
          return swal({
            text: 'Anda sudah meminjam 4 Buku Novel',
          });
        }
      } else if (komik.length >= 3) {
        if (novel.length > 0 || ensiklopedia.length > 0 || komik.length > 3) {
          return swal({
            text: 'Anda sudah meminjam 3 Buku Komik',
          });
        }
      } else if (novel.length >= 3 && komik.length >= 2) {
        if (ensiklopedia.length > 0 || novel.length > 3 || komik.length > 2) {
          return swal({
            text: 'Anda sudah meminjam 3 Buku Novel dan 2 Buku Komik',
          });
        }
      } else if (ensiklopedia.length >= 2) {
        if (komik.length > 0 || novel.length > 0 || ensiklopedia.length > 2) {
          return swal({
            text: 'Anda sudah meminjam 2 Buku Ensiklopedia',
          });
        }
      } else if (
        komik.length > 1 ||
        novel.length > 1 ||
        ensiklopedia.length > 1
      ) {
        return swal({
          text: 'Anda sudah meminjam masing-masing 1 Buku',
        });
      }
    }

    console.log('lolos');
    const url = `http://localhost:8080/updatePinjam/${this.state.token.id_pengguna}`;

    Axios.put(url, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('tokenUserLogin')}`,
      },
    })
      .then(() => {
        swal({
          text: 'Berhasil meminjam buku',
          icon: 'success',
        });
      })
      .catch((err) => console.log(err));
  };

  hapus = (idPinjam, idBuku) => {
    Axios({
      method: 'DELETE',
      url: `http://localhost:8080/deletePinjam/${idPinjam}/${idBuku}`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('tokenUserLogin')}`,
      },
    }).then((res) => {
      alert(res.data);
    });
  };

  render() {
    // const dataBuku = [];
    // for (const [val, index] of this.state.daftarBuku.entries()) {
    //   dataBuku.push(
    //     <>
    //       <div className='cardContainer-keranjang' key={val}>
    //         <div className='card-keranjang'>
    //           <div className='card-name-keranjang'>
    //             <h4>{index.namaPengarang}</h4>
    //             <p className='judulbuku-keranjang'>{index.judulBuku}</p>
    //             <p>{index.hargaSewa}</p>
    //           </div>
    //         </div>
    //       </div>
    //     </>
    //   );
    // }
    return (
      <div>
        <HeaderPengunjung />
        <div className='semua-keranjang'>
          <div className='grid-container-keranjang'>
            {/* {dataBuku} */}
            {this.state.daftarBuku.map((item, i) => {
              return (
                <div className='cardContainer-keranjang' key={i}>
                  <div className='card-keranjang'>
                    <div className='card-image'>
                      <img
                        src={masuk}
                        className='imageContent-pengunjung'
                        alt='login'
                      />
                    </div>

                    <div className='card-name-keranjang'>
                      <h4>{item.produk.namaPengarang}</h4>
                      <p className='judulbuku-keranjang'>
                        {item.produk.judulBuku}
                      </p>
                      <br />
                      <p>{item.produk.hargaSewa}</p>
                      <FontAwesomeIcon
                        icon='trash-alt'
                        onClick={() => this.hapus(item.id, item.produk.id)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <div className='bawah-keranjang'>
            <button onClick={this.pinjam}>Pinjam</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Keranjang;
