import Axios from 'axios';
import React, { Component } from 'react';
import HeaderAdmin from '../Header/HeaderAdmin';
import '../../Admin/Pengguna/EditPengguna.css';
import HeaderPengunjung from '../../Pengunjung/Header/HeaderPengunjung';

const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
// const regexPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,8}$/;
const regexNomor = /^08[0-9]{9,}$/;
const regexUsername = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
export class EditProfil extends Component {
  state = {
    ambilId: JSON.parse(localStorage.getItem('id_pengguna')),
    nama: '',
    alamat: '',
    email: '',
    nomorHp: '',
    status: '',
    username: '',
    password: '',
    tanggalRegis: '',
    namaError: '',
    alamatError: '',
    emailError: '',
    nomorError: '',
    usernameError: '',
  };

  componentDidMount() {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    Axios({
      method: 'GET',
      url: 'http://localhost:8080/getDataPenggunaById/' + this.state.ambilId,
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        this.setState({
          nama: res.data[0].nama,
          alamat: res.data[0].alamat,
          email: res.data[0].email,
          nomorHp: res.data[0].nomorHp,
          status: res.data[0].status,
          username: res.data[0].username,
          password: res.data[0].password,
          tanggalRegis: res.data[0].tanggalRegis,
        });
        console.log(this.state.ambilId);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  //   BATAS SUCI

  handlerSubmit = async (e) => {
    e.preventDefault();
    await Axios.put(
      'http://localhost:8080/updatePengguna/' + this.state.ambilId,
      this.state
    );
    this.props.history.push('/profil');
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  validate = () => {
    let namaError = '';
    let alamatError = '';
    let emailError = '';
    let nomorError = '';
    let usernameError = '';

    if (!this.state.nama) {
      namaError = 'NAMA TIDAK BOLEH KOSONG';
    }

    if (!this.state.alamat) {
      alamatError = 'ALAMAT TIDAK BOLEH KOSONG';
    }

    if (!this.state.email.match(regexEmail)) {
      emailError = 'EMAIL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.nomorHp.match(regexNomor)) {
      nomorError = 'NOMOR PONSEL HARUS SESUAI KETENTUAN';
    }

    if (!this.state.username.match(regexUsername)) {
      usernameError =
        'USERNAME HARUS MEMILIKI MIN 6 KARAKTER DAN TIDAK MENGGUNAKAN SIMBOL';
    }

    if (namaError || alamatError || emailError || nomorError || usernameError) {
      this.setState({
        namaError,
        alamatError,
        emailError,
        nomorError,
        usernameError,
      });
      return false;
    }

    return true;
  };

  render() {
    const {
      nama,
      alamat,
      email,
      nomorHp,
      status,
      username,
      password,
      tanggalRegis,
    } = this.state;
    return (
      <div className='semua-editpengguna'>
        <HeaderPengunjung />
        <h2 className='judul-editpengguna'>Edit Data Pengguna</h2>

        <div className='grid-container-editpengguna'>
          <div className='bagian-kiri-editpengguna'>
            <label>Nama</label>
            <br />
            <input
              type='text'
              name='nama'
              onChange={this.handleChange}
              value={nama}
            />
            <div>{this.state.namaError}</div>
            <br />

            <label>Nomor Telepon</label>
            <br />
            <input
              name='nomorHp'
              type='text'
              onChange={this.handleChange}
              value={nomorHp}
            />
            <div>{this.state.nomorError}</div>

            <br />

            <label>E-Mail</label>
            <br />
            <input
              name='email'
              type='text'
              onChange={this.handleChange}
              value={email}
            />
            <div>{this.state.emailError}</div>

            <label>Alamat</label>
            <br />
            <input
              name='alamat'
              type='text'
              onChange={this.handleChange}
              value={alamat}
            ></input>
            <div>{this.state.alamatError}</div>
            <br />
          </div>

          <div className='bagian-kanan-editpengguna'>
            <label>Username</label>
            <br />
            <input
              name='username'
              type='text'
              onChange={this.handleChange}
              value={username}
            ></input>
            <div>{this.state.usernameError}</div>
            <br />

            <label>Status</label>
            <br />
            <input name='status' type='text' disabled value={status}></input>
            <br />
            <br />

            <label>Tanggal Registrasi</label>
            <br />
            <input
              name='username'
              type='text'
              value={tanggalRegis}
              disabled
            ></input>
            <br />
          </div>
        </div>
        <button onClick={this.handlerSubmit} className='simpan-editpengguna'>
          Simpan
        </button>
      </div>
    );
  }
}

export default EditProfil;
