import React, { Component, Fragment } from 'react';
import masuk from '../../../assets/login.png';
import '../../Admin/Profil/Profil.css';
import Axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HeaderPengunjung from '../Header/HeaderPengunjung';

class ProfilP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      token: '',
    };
  }

  componentDidMount = () => {
    const token = 'Bearer ' + localStorage.getItem('tokenUserLogin');
    const { user } = this.props.security;
    console.log(user.id_pengguna);
    Axios.get(`http://localhost:8080/getDataPenggunaById/` + user.id_pengguna, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    }).then((res) => {
      const users = res.data;
      this.setState({
        users: users,
      });
      console.log(res.data);
    });
  };

  edits = (id_pengguna) => {
    console.log(id_pengguna);
    localStorage.setItem('id_pengguna', id_pengguna);
    window.location.href = '/editprofil';
  };

  ubah = (id_pengguna) => {
    console.log(id_pengguna);
    localStorage.setItem('id_pengguna', id_pengguna);
    window.location.href = '/ubahpassword';
  };


  render() {
    return (
      <Fragment>
        <HeaderPengunjung />

        <div className='halaman-profil-admin'>
          {this.state.users.map((user, idx) => (
            <div className='card-profil-adm'>
              <div className='card-image-profiladm'>
                <img src={masuk} className='imageContent' alt='login' />
              </div>
              <div class='card-nama-profil-adm'>
                <h4>
                  {user.nama} ({user.username})
                </h4>

                <p>{user.email}</p>
                <p>{user.nomorHp}</p>
                <p>{user.status}</p>
                <div className='alamat-profil-adm'>
                  <p>{user.alamat}</p>
                </div>
                <button
                  className='ubah-profil-adm'
                  onClick={() => this.edits(user.id_pengguna)}
                >
                  EDIT DATA
                </button>
                <button className='ubah-password-adm' 
                  onClick={() => this.ubah(user.id_pengguna)}
                >EDIT PASSWORD</button>
              </div>
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

ProfilP.propTypes = {
  security: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(ProfilP);
