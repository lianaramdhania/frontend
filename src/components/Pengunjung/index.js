import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import HomePengunjung from './HomePengunjung';
import TampilBukuP from './Buku/TampilBukuP';
import UbahPassword from '../Admin/Profil/UbahPassword';

class PagePengunjung extends Component {
  render() {
    // const { validToken, user } = this.props.security;
    const { user } = this.props.security;

    if (user.password === '12345') {
      return <Redirect to='/ubahpassword' />;
    }
    if (user.status !== 'Teman' && user.status !== 'Sahabat') {
      return <Redirect to='/admin' />;
    }

    return (
      <>
        <div>
          <Router>
            <Switch>
              <Route exact path='/pengunjung' component={HomePengunjung} />
              <Route exact path='/bukuP' component={TampilBukuP} />
              <Route exact path='/ubahpassword' component={UbahPassword} />
              
            </Switch>
          </Router>
        </div>
      </>
    );
  }
}

PagePengunjung.propTypes = {
  security: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(PagePengunjung);
