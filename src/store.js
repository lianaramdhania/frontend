import { createStore, applyMiddleware } from 'redux';
import combineReducers from '../src/reducers/index';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storageSession from 'redux-persist/lib/storage/session';
import createEncryptor from 'redux-persist-transform-encrypt';
const encryptor = createEncryptor({
  secretKey: 'SECRET-STUFF',
});
const persistConfig = {
  key: 'auth',
  storage: storageSession,
  whitelist: 'security',
  transforms: [encryptor],
};
const pReducer = persistReducer(persistConfig, combineReducers);

const middleware = applyMiddleware(thunk, logger);
const store = createStore(pReducer, middleware);

const persistor = persistStore(store);

export { persistor, store };
